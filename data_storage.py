import tattoo_data_loader.py

"""
Stores all samples from images in pickle format. Needs to be done only once
"""
tattoo_data_loader.store_data_in_pickle()