import PictureRectangle
import os
import cv2
import numpy
import random
import math



class Cutter:
    
    
    def __init__(self):
        self.size = 20
        self.dimension = 10
        self.good_samples = "Dobri_primjeri"
        self.bad_samples = "Losi_primjeri"

    def point_in_poly(self, x,y,poly):
        """
        Checks whether the given point with x and y coordinates is in the given polygon
        """
        n = len(poly)
        inside = False
        # Iterirat mozda od x do x+len/2 i x - len/2 i obrnuto, SAMO ZA NEGATIVNE
        p1x,p1y = poly[0]
        for i in range(n+1):
            p2x,p2y = poly[i % n]
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xints:
                            inside = not inside
            p1x,p1y = p2x,p2y
        return inside

    def find_extremes(self, listOfPolygons):
        """
        Simple method for finding minimums and maximums of each tattoo on image
        """
        minimums = []
        for polygon in listOfPolygons:
            min_x = -1
            min_y = -1
            max_x = -1
            max_y = -1
            for points in polygon:
                x = points[0]
                y = points[1]
                if min_x == -1:
                    min_x, max_x = x, x
                    min_y, max_y = y, y
                else:
                    if x > max_x:
                        max_x = x
                    if x < min_x:
                        min_x = x
                    if y < min_y:
                        min_y = y
                    if y > max_y:
                        max_y = y
            minimums.append((min_x, max_x, min_y, max_y))
        return minimums

    def select_positive_samples(self, listOfPolygons, extremes, file_name):
        polygon_count = 0
        unique_id = 0
        # For each polygon in tattoo we find 25 (or some different number) positive samples
        for polygon in listOfPolygons:
            counter = 0
            while counter < self.size:
                # Randomly select x and y inside boundaries of tattoo
                x = random.randint(extremes[polygon_count][0], extremes[polygon_count][1])
                y = random.randint(extremes[polygon_count][2], extremes[polygon_count][3])
                # Check if randomly selected x and y are indeed inside the tattoo
                statement = self.point_in_poly(x,y,polygon)
                # If they are in tattoo
                if statement:
                    try:
                        # Creates a rectangle from existing image
                        pic = PictureRectangle.PictureRectangle(x,
                                                                y,
                                                                os.path.join(os.path.pardir,
                                                                             os.path.join("Baza_slika_s_tetovazama",
                                                                                          file_name)),
                                                                unique_id, self.dimension)
                        #Saves that to the file
                        pic.save(os.path.join(os.path.pardir, self.good_samples))
                        unique_id += 1
                        counter += 1
                    except Exception as e:
                        continue
            # After iteration, polygon count increases so proper extremities can be fetched
            polygon_count +=1

    def select_negative_samples(self, listOfPolygons, width, height, file_name):
        """
        Method which selects all negative samples from given polygons - tattoos on the image based on the extremes of
        the given tattoos (minimums and maximums).
        """
        polygon_count = 0
        unique_id = 0
        # For each polygon in tattoo we find 25 (or some different number) positive samples
        for polygon in listOfPolygons:
            counter = 0
            while counter < self.size:
                # Randomly select x and y inside boundaries of tattoo
                x = random.randint(0, width)
                y = random.randint(0, height)
                # Check if randomly selected x and y are indeed inside ANY tattoo on image
                for any_poly in listOfPolygons:
                    statement = self.point_in_poly(x,y,any_poly)
                    if statement:
                        break
                # If they are in tattoo
                if not statement:
                    try:
                        # Creates a rectangle from existing image
                        pic = PictureRectangle.PictureRectangle(x,
                                                                y,
                                                                os.path.join(os.path.pardir,
                                                                             os.path.join("Baza_slika_s_tetovazama",
                                                                                          file_name)),
                                                                unique_id, self.dimension)
                        #Saves that to the file
                        pic.save(os.path.join(os.path.pardir, self.bad_samples))
                        unique_id += 1
                        counter += 1
                    except Exception as e:
                        continue
            # After iteration, polygon count increases so proper extremities can be fetched
            polygon_count +=1

    def change_size(self, size):
        self.size = size

    def change_dimension(self, dimension):
        self.dimension = dimension
