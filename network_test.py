"""
network.py
~~~~~~~~~~

A module to implement the stochastic gradient descent learning
algorithm for a feedforward neural network.  Gradients are calculated
using backpropagation.  Note that I have focused on making the code
simple, easily readable, and easily modifiable.  It is not optimized,
and omits many desirable features.
"""

#### Libraries
# Standard library
import random

# Third-party libraries
import numpy as np

class Network(object):

    def __init__(self, sizes, best_weights, best_biases):
        """The list ``sizes`` contains the number of neurons in the
        respective layers of the network.  For example, if the list
        was [2, 3, 1] then it would be a three-layer network, with the
        first layer containing 2 neurons, the second layer 3 neurons,
        and the third layer 1 neuron.  The biases and weights for the
        network are initialized randomly, using a Gaussian
        distribution with mean 0, and variance 1.  Note that the first
        layer is assumed to be an input layer, and by convention we
        won't set any biases for those neurons, since biases are only
        ever used in computing the outputs from later layers."""
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.storage = dict()
        self.biases = best_biases
        self.weights = best_weights

    def feedforward(self, a):
        """Return the output of the network if ``a`` is input."""
        for b, w in zip(self.biases, self.weights):
            a = sigmoid(np.dot(w, a)+b)
        return a


    def SGD(self, training_data, epochs, mini_batch_size, eta,
            test_data=None):
        """Train the neural network using mini-batch stochastic
        gradient descent.  The ``training_data`` is a list of tuples
        ``(x, y)`` representing the training inputs and the desired
        outputs.  The other non-optional parameters are
        self-explanatory.  If ``test_data`` is provided then the
        network will be evaluated against the test data after each
        epoch, and partial progress printed out.  This is useful for
        tracking progress, but slows things down substantially."""
        if test_data: n_test = len(test_data)
        n = len(training_data)
        best = 0
        for j in xrange(epochs):
            if test_data:
                results, uspjeh = self.evaluate(test_data)
                print "Epoch {0}: {1} / {2}".format(
                   j, uspjeh, n_test)
            else:
                print "Epoch {0} complete".format(j)
        return results


    def evaluate(self, test_data):
        #pogledaj kako izgleda taj x
        test_results = [(self.feedforward(x), y, xcoord, ycoord)
                        for (x, y, xcoord, ycoord) in test_data]
        # OVO SLUZI ZA TEST DATU
        #suma = 0
        #for (x,y) in test_results:
        #    if (x >= 0.6 and y == 1.0) or (x < 0.6 and y == 0.0):
        #        suma += 1
        return (test_results, sum(int(x >= 0.7)for (x,y, a, b) in test_results))
        #return sum (int((x>= 0.7 and y == 1.0) or ( x < 0.7 and y == 0.0)) for (x,y) in test_results)
        #return sum (int(x >= 0.6)for (x,y) in test_results)
        #return suma
        #return sum(int(x == y) for (x, y) in test_results)

    def cost_derivative(self, output_activations, y):
        """Return the vector of partial derivatives \partial C_x /
        \partial a for the output activations."""
        return (output_activations-y)


#### Miscellaneous functions
def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))

def sigmoid_prime(z):
    """Derivative of the sigmoid function."""
    return sigmoid(z)*(1-sigmoid(z))

def get_pixel(img, x, y):
    """
    :param img: image from which the pixel needs to be taken
    :param x: x coordinate
    :param y: y coordinate
    :return: RGB value in an array
    """
    return img[y, x]