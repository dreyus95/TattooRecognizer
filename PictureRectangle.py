import cv2
import numpy
import os


class PictureRectangle:
    # name of the picture itself
    name = None
    # id of the rectangle in the picture
    # it is here just for differentiating pictures that came from the same original
    uniqueID = None
    # picture from which this picture originated. not really needed
    img = None
    """
    constructs a 8pixels by 8pixels picture. for now, it isn't made to detect if the marking pixel that is given
    is so close to the end of the picture so it passes the boundaries of the picture. maybe it will need to be
    implemented
    :x x coordinate of the central pixel
    :y y coordinate of the central pixel
    :img image from which the picture will be cut out
    """

    def __init__(self, x, y, img, uniqueID, length):
        (file_path, self.name) = os.path.split(img)
        self.name = self.name.split(".")[0]
        img = cv2.imread(img, cv2.IMREAD_COLOR)

        height, width, channels = img.shape
        if height < y or width < x:
            raise Exception("bad coordinates")

        if y + length / 2 > height:
            y = y - length / 2
        elif y - length / 2 < 0:
            y = y + length / 2
        if x + length / 2 > width:
            x = x - length / 2
        elif x - length / 2 < 0:
            x = x + length / 2

        # check if the picture would be inside the boundaries
        # if not position it so it can be done

        # tu je bila greska
        cut_img = img[y - length / 2: y + length / 2, x - length / 2: x + length / 2]
        self.img = cut_img
        self.uniqueID = uniqueID

    def save(self, save_path):
        cv2.imwrite(os.path.join(save_path, self.name + self.uniqueID.__str__() + ".jpg"), self.img)

    def return_image(self):
        return self.img


def main():
    pic = PictureRectangle(110, 110, os.path.join(os.path.curdir, "simple_tattoo.jpg"), 0)
    pic.save(os.path.join(os.path.pardir, "Dobri_primjeri"))
    print os.path.isdir(os.path.join(os.path.pardir, "Dobri_primjeri"))


if __name__ == "__main__":
    main()
