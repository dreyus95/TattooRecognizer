"""
tattoo_data_loader
~~~~~~~~~~~~

A library to load the tattoo image data.  For details of the data
structures that are returned, see the doc strings for ``load_data``
and ``load_data_wrapper``.  In practice, ``load_data_wrapper`` is the
function usually called by our neural network code.
"""

# Third-party libraries
import numpy as np
import trained_network
import os
import cv2
from random import shuffle
import pickle

numOfExamples = 55000

numOfTest = 11000

#good_dir - path to the good samples
good_dir = os.path.join(os.path.pardir, "Dobri_primjeri")
#bad_dir - path to the bad samples
bad_dir = os.path.join(os.path.pardir, "Losi_primjeri")

def store_data_in_pickle():
    """Return the tattoo data as a tuple containing the training data,
    the validation data, and the test data.

    The ``training_data`` is returned as a tuple with two entries.
    The first entry contains the actual training images.  This is a
    numpy ndarray with ***N*** entries.  Each entry is, in turn, a
    numpy ndarray with 64 values, representing the 8 * 8 = 64
    pixels in a single tattoo part image.

    The second entry in the ``training_data`` tuple is a numpy ndarray
    containing ***N*** entries.  Those entries are just the bit
    values (0 or 1) for the corresponding images contained in the first
    entry of the tuple.

    The ``validation_data`` and ``test_data`` are similar, except
    each contains only 10,000 images.

    This is a nice data format, but for use in neural networks it's
    helpful to modify the format of the ``training_data`` a little.
    That's done in the wrapper function ``load_data_wrapper()``, see
    below.
    """
    

    print 'checkpoint 0'
    good_samples = load_images(good_dir)
    numOfGoodTraining = min(good_samples.__len__(), numOfExamples/2)
    print 'checkpoint 1'
    
    bad_samples = load_images(bad_dir)
    print 'checkpoint 2'
    print len(good_samples)
    print len(bad_samples)
    print numOfGoodTraining
    training_data = []
    print 'checkpoint 3'
    for i in range(0, numOfGoodTraining, 1):
        #1 is indicating it is a pixel inside a tattoo
        training_data.append((transform_picture(good_samples[i]), 1.0))
        training_data.append((transform_picture(bad_samples[i]),0.0))

    print 'checkpoint 4'
    test_data = []

    start = numOfGoodTraining
    end = min(numOfGoodTraining + numOfTest/2, good_samples.__len__())
    for i in range(start, end, 1):
        test_data.append((transform_picture(good_samples[i]), 1.0))
        test_data.append((transform_picture(bad_samples[i]),0.0))
    print 'checkpoint 5'
    print len(training_data), len(test_data)

    #start = numOfBadTraining
    #end = min(numOfBadTraining + numOfTest/2, bad_samples.__len__())
    #for i in range(start, end, 1):
    #    test_data.append((good_samples[i], 0))

    #for now it's empty, maybe it will be of some use one day
    save_pickle(training_data, test_data)


def load_images(directory):
    images = []
    lista = os.listdir(directory)
    shuffle(lista)
    for filename in lista:
        img = cv2.imread(os.path.join(directory, filename))
        if img is not None:
            images.append(img)
    return images

def transform_picture(img):
    transformed_image = []
    for row in img:
        for pixel in row:
            transformed_image.append(pixel[0]/255.0)
            transformed_image.append(pixel[1]/255.0)
            transformed_image.append(pixel[2]/255.0)

    return np.reshape(transformed_image, (len(transformed_image), 1))

def save_pickle(training_data, test_data):
    """
    Stores all samples of images in pickle file
    """
    fsave = open("data", "w")
    data = [training_data, test_data]
    pickle.dump(data, fsave)

def load_pickle():
    """
    Loads the data from pickle file and shuffles it, so every time the neural
    network is being learned, it will get different files
    """
    fload = open("data", "r")
    data = pickle.load(fload)
    training_data = data[0]
    shuffle(training_data)
    test_data = data[1]
    shuffle(test_data)
    return training_data, test_data


def main():
    import network
    print 'start'
    training_data, test_data = load_pickle()
    print 'end'
    net = network.Network([10*10*3, 40, 1])
    net.SGD(training_data, 50, 10, 3.0, test_data=test_data)
    net.save_best()
    data = network.load_best()

    print "best_biases"
    #print data[0]
    print "best_weights"
   # print data[1]

if __name__ == "__main__":
    main()