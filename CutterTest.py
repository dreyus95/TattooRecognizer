import PictureRectangle
import os
import cv2
import numpy
import random
import math


class Cutter:

    good_samples = "Dobri_primjeri"
    bad_samples = "Losi_primjeri"

    def __init__(self, parent=None):
        super(Cutter, self).__init__(parent)

    def point_in_poly(self, x,y,poly):
        """
        Checks whether the given point with x and y coordinates is in the given polygon
        """
        n = len(poly)
        inside = False

        p1x,p1y = poly[0]
        for i in range(n+1):
            p2x,p2y = poly[i % n]
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xints:
                            inside = not inside
            p1x,p1y = p2x,p2y
        return inside

    def find_extremes(self, listOfPolygons):
        """Simple method for finding minimums and maximums of each tattoo on image
        """
        minimums = []
        for polygon in listOfPolygons:
            min_x = -1
            min_y = -1
            max_x = -1
            max_y = -1
            for points in polygon:
                x = points[0]
                y = points[1]
                if min_x == -1:
                    min_x, max_x = x, x
                    min_y, max_y = y, y
                else:
                    if x > max_x:
                        max_x = x
                    if x < min_x:
                        min_x = x
                    if y < min_y:
                        min_y = y
                    if y > max_y:
                        max_y = y
            minimums.append((min_x, max_x, min_y, max_y))
        return minimums

    def select_positive_samples(self, listOfPolygons, extremes, file_name):
        polygon_count = 0
        unique_id = 0
        # For each polygon in tattoo we find 25 (or some different number) positive samples
        for polygon in listOfPolygons:
            counter = 0
            while counter < 25:
                # Randomly select x and y inside boundaries of tattoo
                x = random.randint(extremes[polygon_count][0], extremes[polygon_count][1])
                y = random.randint(extremes[polygon_count][2], extremes[polygon_count][3])
                # Check if randomly selected x and y are indeed inside the tattoo
                statement = self.point_in_poly(x,y,polygon)
                # If they are in tattoo
                if statement:
                    try:
                        # Creates a rectangle from existing image
                        pic = PictureRectangle.PictureRectangle(x,
                                                                y,
                                                                os.path.join(os.path.pardir,
                                                                             os.path.join("Baza_slika_s_tetovazama",
                                                                                          file_name)),
                                                                unique_id)
                        #Saves that to the file
                        pic.save(os.path.join(os.path.pardir, good_samples))
                        unique_id += 1
                        counter += 1
                    except Exception as e:
                        continue
            # After iteration, polygon count increases so proper extremities can be fetched
            polygon_count +=1

    def select_negative_samples(self, listOfPolygons, extremes, file_name):
        """Method which selects all negative samples from given polygons - tattoos on the image based on the extremes of
        the given tattoos (minimums and maximums)."""
        polygon_count = 0
        unique_id = 0
        # For each polygon in tattoo we find 25 (or some different number) positive samples
        for polygon in listOfPolygons:
            counter = 0
            while counter < 25:
                # Randomly select x and y inside boundaries of tattoo
                x = random.randint(extremes[polygon_count][0], extremes[polygon_count][1])
                y = random.randint(extremes[polygon_count][2], extremes[polygon_count][3])
                # Check if randomly selected x and y are indeed inside the tattoo
                statement = point_in_poly(x,y,polygon)
                # If they are in tattoo
                if not statement:
                    try:
                        # Creates a rectangle from existing image
                        pic = PictureRectangle.PictureRectangle(x,
                                                                y,
                                                                os.path.join(os.path.pardir,
                                                                             os.path.join("Baza_slika_s_tetovazama",
                                                                                          file_name)),
                                                                unique_id)
                        #Saves that to the file
                        pic.save(os.path.join(os.path.pardir, bad_samples))
                        unique_id += 1
                        counter += 1
                    except Exception as e:
                        continue
            # After iteration, polygon count increases so proper extremities can be fetched
            polygon_count +=1

# for all the marked pictures
# take the pixels that are present in the tattoos.txt and make them good examples
# that will be used for the neural network
tattoo_db = open("tattoos.txt")
marked_pictures = tattoo_db.readlines()
count = 0
cutter = Cutter()
for marked_picture in marked_pictures:
    file_name = marked_picture.split(' ')[0].strip()

    # Splitting points so that I can get only [ .... ] in each part of array
    points_splitted = marked_picture.split(' ', 1)[1].split('\t')
    pts = []
    listOfPolygons = []
    for points in points_splitted:
        if not points.strip():
            break
        # getting only what's inside [ and ]
        list_of_points = points[1:len(points.strip())-2]
        # Getting points in format ( ) ( )
        points_in_bracket = list_of_points.split(' ')
        for coordinates in points_in_bracket:
            coordinates = coordinates[1:len(coordinates)-1]
            # Casting coordinates to int right away
            x = int(round(float(coordinates.split(',')[0].strip())))
            y = int(round(float(coordinates.split(',')[1].strip())))
            pts.append((x, y))
        listOfPolygons.append(pts[:])
        del pts[:]

    # Finding minimums and maximums of each tattoo
    extremes = cutter.find_extremes(listOfPolygons)
    cutter.select_positive_samples(listOfPolygons, extremes, file_name)
    cutter.select_negative_samples(listOfPolygons, extremes, file_name)
    count +=1
    # Just so program doesn't run long time
    if count > 15:
        break


# the same has to be done for the bad pics
