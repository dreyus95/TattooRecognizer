import PictureRectangle
import os

# for all the marked pictures
# take the pixels that are present in the tattoos.txt and make them good examples
# that will be used for the neural network
tattoo_db = open("tattoos.txt")
marked_pictures = tattoo_db.readlines()
count = 0
for marked_picture in marked_pictures:
    file_name = marked_picture.split(' ')[0].strip()

    # Splitting points so that I can get only [ .... ] in each part of array
    points_splitted = marked_picture.split(' ', 1)[1].split('\t')

    for points in points_splitted:
        if not points.strip():
            break
        # getting only what's inside [ and ]
        list_of_points = points[1:len(points.strip()) - 2]
        # Getting points in format ( ) ( )
        points_in_bracket = list_of_points.split(' ')
        unique_id = 0
        for coordinates in points_in_bracket:
            coordinates = coordinates[1:len(coordinates) - 1]
            x = float(coordinates.split(',')[0].strip())
            y = float(coordinates.split(',')[1].strip())

            try:
                pic = PictureRectangle.PictureRectangle(int(x),
                                                        int(y),
                                                        os.path.join(os.path.pardir,
                                                                     os.path.join("Baza_slika_s_tetovazama",
                                                                                  file_name)),
                                                        unique_id)
                pic.save(os.path.join(os.path.pardir, "Dobri_primjeri"))
                unique_id += 1
            except Exception as e:
                continue
    count +=1
    if count > 8:
        break
# the same has to be done for the bad pics
