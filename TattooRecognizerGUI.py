# -*- coding: latin-1 -*-
from PyQt4.QtGui import *
from PyQt4.QtGui import QLineEdit
import os
import cv2
from Cutter import Cutter
import TattooSelector
from TattooSelector import TattooSelector
import tattoo_data_loader
import network
import trained_network


class TattooRecognizerGUI(QWidget):
    def __init__(self, parent=None):
        super(TattooRecognizerGUI, self).__init__(parent)
        self.filesInDir = []
        self.currentFileName = ""
        self.directoryName = ""
        self.name_for_network = ""
        [best_biases, best_weights] = network.load_best()
        self.trained_network = trained_network.Network([10 * 10 * 3, 40, 1], best_biases, best_weights)
        self.cutter = Cutter()
        # setting usage instructions
        leftClickInstructionText = "Left click to mark the points of the tattoo"
        rightClickInstructionText = "Right click to remove point"
        nextTattooInstructionText = "Press Next tattoo in order to start marking next tattoo on the same picture"
        saveTattooInstructionText = "Press Save tattoo marking for saving the current progress"
        loadTattooInstructionText = "Press Load tattoo for loading another image"
        selectFolderInstruction = "select folder where you can walk through images and mark them"

        instructionText = leftClickInstructionText + "\n" \
                          + rightClickInstructionText + "\n" \
                          + nextTattooInstructionText + "\n" \
                          + saveTattooInstructionText + "\n" \
                          + loadTattooInstructionText + "\n" \
                          + selectFolderInstruction

        instructionsBox = QTextEdit(self)
        instructionsBox.setDisabled(True)
        instructionsBox.setText(instructionText)
        instructionsBox.setFixedSize(500, 80)

        nextTattooButton = QPushButton("Mark next tattoo on image")
        nextTattooButton.clicked.connect(self.nextTattooButtonClicked)

        saveButton = QPushButton("Save tattoo marking")
        saveButton.clicked.connect(self.saveButtonClicked)

        loadButton = QPushButton("Load tattoo")
        loadButton.clicked.connect(self.loadButtonClicked)

        selectFolderButton = QPushButton("Select folder")
        selectFolderButton.clicked.connect(self.selectFolderClicked)

        nextImageButton = QPushButton("Next image in directory")
        nextImageButton.clicked.connect(self.nextImageInDirectory)

        nextTenButton = QPushButton("Next 10 in directory")
        nextTenButton.clicked.connect(self.nextTenInDirectory)

        previousImageButton = QPushButton("Previous image in directory")
        previousImageButton.clicked.connect(self.previousImageInDirectory)

        previousTenButton = QPushButton("Previous 10 in directory")
        previousTenButton.clicked.connect(self.previousTenInDirectory)

        clearButton = QPushButton("Clear tattoos")
        clearButton.clicked.connect(self.clear_button_clicked)

        quitButton = QPushButton("Quit")
        quitButton.clicked.connect(self.quitButtonClicked)

        select_positives_button = QPushButton("Select positive samples")
        select_positives_button.clicked.connect(self.generate_positive_samples)

        select_negatives_button = QPushButton("Select negative samples")
        select_negatives_button.clicked.connect(self.generate_negative_samples)

        change_sample_size = QPushButton("Change ammount of samples")
        change_sample_size.clicked.connect(self.show_input_for_size)

        select_samples_from_all = QPushButton("Select samples from all images")
        select_samples_from_all.clicked.connect(self.select_all)

        change_sample_dimension = QPushButton("Change dimensions of samples")
        change_sample_dimension.clicked.connect(self.show_input_for_dimension)

        discover_tattoo_button = QPushButton("Find tattoos on image")
        discover_tattoo_button.clicked.connect(self.find_tattoo)

        self.tattooSelector = TattooSelector()

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(instructionsBox)

        childLayout = QHBoxLayout()

        # previous buttons
        leftSide = QWidget()
        leftSideLayout = QVBoxLayout()
        leftSideLayout.addWidget(previousImageButton)
        leftSideLayout.addWidget(previousTenButton)
        leftSide.setLayout(leftSideLayout)
        childLayout.addWidget(leftSide)

        childLayout.addWidget(self.tattooSelector.view)

        # next and previous image in folder buttons
        rightSide = QWidget()
        rightSideLayout = QVBoxLayout()
        rightSideLayout.addWidget(nextImageButton)
        rightSideLayout.addWidget(nextTenButton)
        rightSide.setLayout(rightSideLayout)
        childLayout.addWidget(rightSide)

        mainLayout.addLayout(childLayout)

        buttons_layout = QGridLayout()
        buttons_layout.addWidget(nextTattooButton, 0, 0)
        buttons_layout.addWidget(saveButton, 0, 1)
        buttons_layout.addWidget(loadButton, 1, 0)
        buttons_layout.addWidget(selectFolderButton, 1, 1)
        buttons_layout.addWidget(clearButton, 2, 0)
        buttons_layout.addWidget(quitButton, 2, 1)
        buttons_layout.addWidget(select_positives_button, 3, 0)
        buttons_layout.addWidget(select_negatives_button, 3, 1)
        mainLayout.addLayout(buttons_layout)
        # cropped images
        cropped_layout = QHBoxLayout()

        cropped_layout.addWidget(change_sample_size)
        cropped_layout.addWidget(select_samples_from_all)
        cropped_layout.addWidget(change_sample_dimension)

        find_tattoo_layout = QVBoxLayout()
        find_tattoo_layout.addWidget(discover_tattoo_button)

        mainLayout.addLayout(cropped_layout)
        mainLayout.addLayout(find_tattoo_layout)
        print 'current ammount of samples is ' + str(self.cutter.size)
        print 'current dimensions of samples are ' + str(self.cutter.dimension) + 'x' + str(self.cutter.dimension)
        self.setLayout(mainLayout)
        self.setWindowTitle("Tattoo Recognizer")
        self.resize(500, 500)

    def find_tattoo(self):
        if not self.name_for_network:
            return
        print '1 provjera'
        # img = cv2.imread(os.path.join(self.directoryName.__str__(), self.currentFileName.__str__()))
        img = cv2.imread(self.name_for_network.__str__())
        data = []
        height, width = img.shape[:2]
        dim = self.cutter.dimension
        x = dim / 2
        while x <= width - dim / 2:
            y = dim / 2
            while y < height - dim / 2:
                pic_rec = img[y - dim / 2: y + dim / 2, x - dim / 2: x + dim / 2]
                transformed_square = tattoo_data_loader.transform_picture(pic_rec)
                data.append((transformed_square, 0.5, x, y))
                y += dim / 2
            x += dim
        print '2 provjera'

        # neural_net = network.Network([10*10*3, 100, 1])
        result, ammount_tattood = self.trained_network.evaluate2(test_data=data)
        # print "{1} / {2}".format(ammount_tattood, 2)
        for (value, given, x, y) in result:
            if (value >= 0.90):
                cv2.rectangle(img, (x, y), (x + 10, y + 10), (0, 255, 0), -1)
        cv2.imshow('image', img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def loadFirstImageInDir(self):
        self.currentFileName = self.filesInDir[0]
        self.name_for_network = os.path.join(self.directoryName.__str__(), self.currentFileName.__str__())
        self.load_image(os.path.join(self.directoryName.__str__(), self.currentFileName.__str__()))

    def nextImageInDirectory(self):
        if not self.filesInDir:
            return
        self.currentFileName = self.filesInDir[self.filesInDir.index(self.currentFileName.__str__()) + 1]
        self.name_for_network = os.path.join(self.directoryName.__str__(), self.currentFileName.__str__())
        self.load_image(os.path.join(self.directoryName.__str__(), self.currentFileName.__str__()))

    def nextTenInDirectory(self):
        if not self.filesInDir:
            return
        self.currentFileName = self.filesInDir[self.filesInDir.index(self.currentFileName.__str__()) + 10]
        self.name_for_network = os.path.join(self.directoryName.__str__(), self.currentFileName.__str__())
        self.load_image(os.path.join(self.directoryName.__str__(), self.currentFileName.__str__()))

    def previousImageInDirectory(self):
        if not self.filesInDir:
            return
        if self.filesInDir.index(self.currentFileName) == 0:
            return
        self.currentFileName = self.filesInDir[self.filesInDir.index(self.currentFileName.__str__()) - 1]
        self.load_image(os.path.join(self.directoryName.__str__(), self.currentFileName.__str__()))

    def previousTenInDirectory(self):
        if not self.filesInDir:
            return
        if self.filesInDir.index(self.currentFileName) == 0:
            return
        self.currentFileName = self.filesInDir[self.filesInDir.index(self.currentFileName.__str__()) - 10]
        self.load_image(os.path.join(self.directoryName.__str__(), self.currentFileName.__str__()))

    def selectFolderClicked(self):
        fileDialog = QFileDialog()
        name = fileDialog.getExistingDirectory(self, 'Select Directory with Images')
        self.directoryName = name
        self.filesInDir = os.listdir(name)
        self.loadFirstImageInDir()

    def saveButtonClicked(self):
        """
    Saves all the tattoos on the image in the text file. If user didn't click next tattoo button
    then the currently stored tattoo will be stored so that proper informations will be
    written in the text file.
    :return: none
    """
        if not self.tattooSelector.name:
            return
        sender = self.sender()
        print (sender.text() + ' was pressed')
        # If there is a tattoo that isn't stored in list of tattoos then that tattoo needs to be stored
        # That is possible with invoking function next_tattoo
        if self.tattooSelector.points:
            self.tattooSelector.next_tattoo()
        self.tattooSelector.write_polygons_to_file()

    def loadButtonClicked(self):
        """
    Allows the user to choose which photo he would like to select. That photo will be available
    for selection of tattoos.
    :return: none
    """
        sender = self.sender()
        print (sender.text() + ' was pressed')
        fileDialog = QFileDialog()
        name = fileDialog.getOpenFileName(self, 'Select file')
        self.name_for_network = name.__str__()
        print self.name_for_network
        self.load_image(name.__str__())

    def generate_positive_samples(self):
        if not self.tattooSelector.name:
            return
        print 'generating positive samples for ' + self.tattooSelector.name
        temp_list = self.tattooSelector.cast_to_int()
        extremes = self.cutter.find_extremes(temp_list)
        self.cutter.select_positive_samples(temp_list, extremes, self.tattooSelector.name)
        print 'Done!'

    def generate_negative_samples(self):
        if not self.tattooSelector.name:
            return
        print 'generating negative samples for ' + self.tattooSelector.name
        temp_list = self.tattooSelector.cast_to_int()
        extremes = self.cutter.find_extremes(temp_list)
        img = cv2.imread(
            os.path.join(os.path.pardir, os.path.join("Baza_slika_s_tetovazama", self.tattooSelector.name)),
            cv2.IMREAD_COLOR)
        height, width, channels = img.shape
        self.cutter.select_negative_samples(temp_list, width, height, self.tattooSelector.name)
        print 'Done!'

    def show_input_for_size(self):
        text, ok = QInputDialog.getText(self, 'Input Dialog', 'Enter new size: ')
        text = str(text)
        try:
            size = int(text)
        except Exception as ex:
            return
        print 'New size is ' + text
        self.cutter.change_size(size)

    def select_all(self):
        lines = self.tattooSelector.return_all_lines()
        count = 0
        print 'Started cutting samples from all images!'
        for marked_picture in lines:
            # if count <= 839:
            #   count +=1
            #   continue;
            file_name = marked_picture.split(' ')[0].strip()
            img = cv2.imread(os.path.join(os.path.pardir, os.path.join("Baza_slika_s_tetovazama", file_name)),
                             cv2.IMREAD_COLOR)
            height, width, channels = img.shape
            # Splitting points so that I can get only [ .... ] in each part of array
            points_splitted = marked_picture.split(' ', 1)[1].split('\t')
            pts = []
            listOfPolygons = []
            for points in points_splitted:
                if not points.strip():
                    break
                # getting only what's inside [ and ]
                list_of_points = points[1:len(points.strip()) - 2]
                # Getting points in format ( ) ( )
                points_in_bracket = list_of_points.split(' ')
                if len(points_in_bracket) == 0:
                    break
                for coordinates in points_in_bracket:
                    coordinates = coordinates[1:len(coordinates) - 1]
                    # Casting coordinates to int right away
                    x = int(round(float(coordinates.split(',')[0].strip())))
                    y = int(round(float(coordinates.split(',')[1].strip())))
                    pts.append((x, y))
                listOfPolygons.append(pts[:])
                del pts[:]

            # Finding minimums and maximums of each tattoo
            extremes = self.cutter.find_extremes(listOfPolygons)
            self.cutter.select_positive_samples(listOfPolygons, extremes, file_name)
            self.cutter.select_negative_samples(listOfPolygons, width, height, file_name)
            # Just so program doesn't run long time
            count += 1
            print 'Done with ' + str(count)
        print 'Done!'

    def show_input_for_dimension(self):
        text, ok = QInputDialog.getText(self, 'Input Dialog', 'Enter new size: ')
        text = str(text)
        try:
            dimension = int(text)
        except Exception as ex:
            return
        print 'New dimensions are ' + text + 'x' + text
        self.cutter.change_dimension(dimension)

    def load_image(self, name):
        self.tattooSelector.read_image(name=name)
        self.tattooSelector.read_polygons_from_file(name=name)

    def nextTattooButtonClicked(self):
        """
    Currently selected tattoo will be stored and remembered, user can select a new tattoo
    on the same image.
    :return: none
    """
        self.tattooSelector.next_tattoo()

    def clear_button_clicked(self):
        """
    Clears all the local storage of tattoos.
    :return:
    """
        self.tattooSelector.clear_storage()

    def quitButtonClicked(self):
        """
    When quit is pressed closes the app.
    :return: none
    """
        self.close()


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    gui = TattooRecognizerGUI()
    gui.show()
    sys.exit(app.exec_())
