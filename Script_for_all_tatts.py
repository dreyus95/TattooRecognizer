
from Cutter import Cutter



# for all the marked pictures
# take the pixels that are present in the tattoos.txt and make them good examples
# that will be used for the neural network
tattoo_db = open("tattoos.txt")
marked_pictures = tattoo_db.readlines()
count = 0
for marked_picture in marked_pictures:
    file_name = marked_picture.split(' ')[0].strip()

    # Splitting points so that I can get only [ .... ] in each part of array
    points_splitted = marked_picture.split(' ', 1)[1].split('\t')
    pts = []
    listOfPolygons = []
    for points in points_splitted:
        if not points.strip():
            break
        # getting only what's inside [ and ]
        list_of_points = points[1:len(points.strip())-2]
        # Getting points in format ( ) ( )
        points_in_bracket = list_of_points.split(' ')
        for coordinates in points_in_bracket:
            coordinates = coordinates[1:len(coordinates)-1]
            # Casting coordinates to int right away
            x = int(round(float(coordinates.split(',')[0].strip())))
            y = int(round(float(coordinates.split(',')[1].strip())))
            pts.append((x, y))
        listOfPolygons.append(pts[:])
        del pts[:]

    # Finding minimums and maximums of each tattoo
    extremes = find_extremes(listOfPolygons)
    select_positive_samples(listOfPolygons, extremes, file_name)
    select_negative_samples(listOfPolygons, extremes, file_name)
    count +=1
    # Just so program doesn't run long time
    if count > 15:
        break


# the same has to be done for the bad pics