# coding=utf-8
import sys
from PyQt4 import QtCore
from PyQt4.QtGui import *
import os

class TattooSelector(QWidget):
    """
    Name of the file where all tattoos are stored
    """
    #this way we get relative path to our tattoos.txt file
    dir = os.path.dirname(__file__)
    FILE_NAME = os.path.join(dir,"tattoos.txt")
    def __init__(self):
        """
        Constructor of this class
        :return: none
        """
        super(TattooSelector, self).__init__()
        self.points = []
        self.listOfPolygons = []
        self.pixmap = None
        self.name = None
        #self.pixmap = QPixmap()
        #ovaj dio sa pixmap-om ne bi smio biti vezan za pojedinačnu sliku već za neku koja je izabrana
        self.scene = QScene(self)
        self.view = QView(self.scene)
        layout = QVBoxLayout()
        self.setLayout(layout)
        #self.scene.addPixmap(self.pixmap)
        self.view.setScene(self.scene)
        #self.view.show()

    def read_image(self, name):
        """
        Sets current image based on what user selected in File chooser dialog.
        :param name: Name of image which will be loaded
        :return: nothing
        """
        if not name:
            return
        items = self.scene.items()
        for item in items:
            self.scene.removeItem(item)
        self.pixmap = QPixmap(name)
        #get the filepath and filename
        (filepath, filename) = os.path.split(name)
        self.name = filename
        self.scene.addPixmap(self.pixmap)
        #self.view.setMinimumSize(self.pixmap.size())
        self.clear_storage()

    def clear_storage(self):
        """
        Deletes all currently stored tattoos and points of them
        :return: none
        """
        del self.points[:]
        del self.listOfPolygons[:]
        if self.pixmap:
            self.scene.addPixmap(self.pixmap)

    def next_tattoo(self):
        """
        Stores current tattoo in a list of tattoos and clears the list which contained points of tattoo.
        :return: none
        """
        self.listOfPolygons.append(self.points[:])
        del self.points[:]

    def draw_polygon(self):
        """
        Draws all the tattoos to the image (all the polygons).
        :return: none
        """
        for tattooPoints in self.listOfPolygons:
            pen = QPen()
            pen.setColor(QtCore.Qt.red)
            tempX = -1
            tempY = -1
            for xDot, yDot in tattooPoints:
                self.scene.addEllipse(xDot-1, yDot-1, 2, 2, pen)
                if tempX == -1:
                    tempX = xDot
                    tempY = yDot
                else:
                    self.scene.addLine(tempX, tempY, xDot, yDot, pen)
                    tempX = xDot
                    tempY = yDot

    def write_polygons_to_file(self):
        """
        Writes all the polygons that are currently stored in the application
        to the "database" which is represented by a textual file.
        :return:
        """
        f = None
        if not self.name:
            return
        try:
            f = open(self.FILE_NAME, "r+")
        except IOError as e:
            f = open(self.FILE_NAME, "a+")

        tattoo_info = f.readlines()
        f.close()
        temp_list = []
        flag = False
        for line in tattoo_info:
            if line.startswith(self.name):
                flag = True
                continue
            else:
                temp_list.append(line)

        if flag:
            open(self.FILE_NAME, "w").close()
            if temp_list:
                f = open(self.FILE_NAME, "a")
                f.writelines(temp_list)
        if f.closed:
            f = open(self.FILE_NAME, "a")
        f.write(self.name + ' ')
        for list in self.listOfPolygons:
            f.write('[')
            for x, y in list:
                f.write('(' + str(x) + ',' + str(y) + ') ')
            f.write(']\t')
        f.write('\n')
        f.close()

    def cast_to_int(self):
        """ Returns the list of current tattoos on image casted to integers """
        temp_list, pts, temp_polygons = [], [], []
        for polygon in self.listOfPolygons:
            for x, y in polygon:
                x = int(x)
                y = int(y)
                pts.append((x,y))
            temp_list.append(pts[:])
            del pts[:]
        return temp_list

    def return_all_lines(self):
        """Returns all lines from database"""
        f = open(self.FILE_NAME)
        lines = f.readlines()
        f.close()
        return lines

    def read_polygons_from_file(self, name):
        """
        Reads all tattoos on one image and draws them on the image.
        :param name: Name of the file that has to be found in file containing informations about tattos on images
        :return:
        """
        if (not name) or (not self.name):
            return
        (filepath, filename) = os.path.split(name)
        name = filename
        f = open(self.FILE_NAME)
        lines = f.readlines()
        f.close()
        pts = []
        points_string = ""
        for line in lines:
            file_name = line.split(' ')[0].strip()
            if file_name == name:
                # Getting points in format [ .... ] [ .... ]
                points_string = line.split(' ', 1)[1]
                break
        if not points_string:
            return

        # Splitting points so that I can get only [ .... ] in each part of array
        points_splitted = points_string.split('\t')
        for points in points_splitted:
            if not points.strip():
                break
            # getting only what's inside [ and ]
            list_of_points = points[1:len(points.strip())-2]
            # Getting points in format ( ) ( )
            points_in_bracket = list_of_points.split(' ')
            for coordinates in points_in_bracket:
                coordinates = coordinates[1:len(coordinates)-1]
                x = float(coordinates.split(',')[0].strip())
                y = float(coordinates.split(',')[1].strip())
                pts.append((x, y))
            self.listOfPolygons.append(pts[:])
            del pts[:]
        if self.listOfPolygons:
            self.draw_polygon()

class QView(QGraphicsView):

    def __init__(self, QScene):
        QGraphicsView.__init__(self)
        self.scene = QScene

    def resizeEvent(self, QResizeEvent):
        """
        Method which resizes the photo properly
        :param QResizeEvent: Event that occurs during resizing of scene.
        :return: none
        """
        self.fitInView(self.scene.itemsBoundingRect())

    def wheelEvent(self, event):
        # Zoom Factor
        zoomInFactor = 1.25
        zoomOutFactor = 1 / zoomInFactor

        # Ovo se mozda moze maknuti
        self.setTransformationAnchor(QGraphicsView.NoAnchor)
        self.setResizeAnchor(QGraphicsView.NoAnchor)

        # Save the scene pos
        oldPos = self.mapToScene(event.pos())

        # Zoom
        if event.delta() > 0:
            zoomFactor = zoomInFactor
        else:
            zoomFactor = zoomOutFactor
        self.scale(zoomFactor, zoomFactor)

        # Get the new position
        newPos = self.mapToScene(event.pos())

        # Move scene to old position
        delta = newPos - oldPos
        self.translate(delta.x(), delta.y())

class QScene(QGraphicsScene):
    def __init__(self, TattooSel):
        QGraphicsScene.__init__(self)
        self.tattooSelector = TattooSel

    def mousePressEvent(self, QGraphicsSceneMouseEvent):
        """
        Method which is called when user clicks mouse button. If left mouse button is clicked
        then new point of tattoo will be stored, if the right button is pressed, then
        last point of the current tattoo will be deleted.
        :param QGraphicsSceneMouseEvent: Event which occurs during mouse click.
        :return: none
        """
        if not self.tattooSelector.pixmap:
            return
        x = QGraphicsSceneMouseEvent.scenePos().x()
        y = QGraphicsSceneMouseEvent.scenePos().y()
        if QGraphicsSceneMouseEvent.button() == QtCore.Qt.LeftButton:
            self.tattooSelector.points.append((x, y))
            self.draw_items()
        if QGraphicsSceneMouseEvent.button() == QtCore.Qt.RightButton:
            if not self.tattooSelector.points:
                return
            if len(self.tattooSelector.points) > 0:
                del self.tattooSelector.points[-1]
            self.addPixmap(self.tattooSelector.pixmap)
            self.draw_items()

    #def wheelEvent(self, QGraphicsSceneWheelEvent):
        # TODO

    def draw_items(self):
        pen = QPen()
        pen.setColor(QtCore.Qt.red)
        for list in self.tattooSelector.listOfPolygons:
            tempX = -1
            tempY = -1
            for xDot, yDot in list:
                # xDot and yDot are adjusted because of the way Ellipse is drawn
                self.addEllipse(xDot-1, yDot-1, 2, 2, pen)
                if tempX == -1:
                    tempX = xDot
                    tempY = yDot
                else:
                    self.addLine(tempX, tempY, xDot, yDot, pen)
                    tempX = xDot
                    tempY = yDot
        if self.tattooSelector.points:
            tempX = -1
            tempY = -1
            for xDot, yDot in self.tattooSelector.points:
                # xDot and yDot are adjusted because of the way Ellipse is drawn
                self.addEllipse(xDot-1, yDot-1, 2, 2, pen)
                if tempX == -1:
                    tempX = xDot
                    tempY = yDot
                else:
                    self.addLine(tempX, tempY, xDot, yDot, pen)
                    tempX = xDot
                    tempY = yDot

def main():
    
    app = QApplication(sys.argv)
    widget = TattooSelector()
    widget.view.show()
    app.exec_()
    sys.exit()

if __name__ == '__main__':
    main()